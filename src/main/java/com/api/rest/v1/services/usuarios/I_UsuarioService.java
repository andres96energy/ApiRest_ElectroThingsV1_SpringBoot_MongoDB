package com.api.rest.v1.services.usuarios;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import com.api.rest.v1.entities.UsuarioEntity;

public interface I_UsuarioService {
	

	// ===============================================
	// ============= MÉTODOS HTTP CRUD ==============
	// ===============================================

	public abstract void addUsuario(UsuarioEntity usuario);

	public abstract void updateUsuario(UsuarioEntity usuario);

	public abstract void deleteUsuario(String id);

	public abstract Page<UsuarioEntity> getAllUsuarios(Pageable pageable);

	// ==================================================
	// ============= MÉTODOS HTTP BÚSQUEDA =============
	// ==================================================

	public abstract UsuarioEntity getById(String id);

	public abstract Page<UsuarioEntity> getByNombre(String nombre, Pageable pageable);
	
	public abstract Page<UsuarioEntity> getByApellido(String apellido, Pageable pageable);
	
	public abstract Page<UsuarioEntity> getByUser(String user, Pageable pageable);
	
	public abstract UsuarioEntity getByUser(String user);
	
	public abstract Page<UsuarioEntity> getByPassword(String password, Pageable pageable);

	public abstract Page<UsuarioEntity> getByRol(String rol, Pageable pageable);
	

}
